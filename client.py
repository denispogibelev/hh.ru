import asyncio
import pickle

class Client:
    def __init__(self, start=0, end=10):
        self.range = (start, end)
        self.sort = None

    async def refresh(self):
        reader, writer = await asyncio.open_connection('127.0.0.1', 8888)
        request = {'range': self.range, 'sort': self.sort}
        writer.write(pickle.dumps(request))
        data = await reader.read(100)
        rows = pickle.loads(data)
        writer.close()
        await writer.wait_closed()
        self.display(rows)

    def display(self, rows):
        print(rows)

client = Client()
asyncio.run(client.refresh())