import asyncio
import pickle
from collections import OrderedDict

class Table:
    def __init__(self):
        self.data = OrderedDict()  # идентификатор: строка

    def insert(self, id, row):
        self.data[id] = row

    def delete(self, id):
        del self.data[id]

    def update(self, id, row):
        self.data[id] = row

    def get_rows(self, start, end):
        ids = list(self.data.keys())[start:end]
        rows = [self.data[id] for id in ids]
        return rows

    def sort_by_column(self, column, reverse=False):
        self.data = OrderedDict(sorted(self.data.items(), key=lambda x: x[1][column], reverse=reverse))

table = Table()

async def handle_client(reader, writer):
    while True:
        data = await reader.read(100)
        request = pickle.loads(data)
        start, end = request['range']
        if 'sort' in request:
            column, reverse = request['sort']
            table.sort_by_column(column, reverse)
        rows = table.get_rows(start, end)
        writer.write(pickle.dumps(rows))
        await writer.drain()

async def main():
    server = await asyncio.start_server(handle_client, '127.0.0.1', 8888)
    async with server:
        await server.serve_forever()

asyncio.run(main())